package com.project.hsTan.dao;

import com.project.hsTan.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    List<String> test();

    List<User> getList();
}
