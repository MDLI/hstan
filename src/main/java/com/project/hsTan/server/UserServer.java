package com.project.hsTan.server;

import com.project.hsTan.dao.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServer {

    @Resource
    private UserMapper userMapper;

    public List<String> test() {
        return userMapper.test();
    }
}
