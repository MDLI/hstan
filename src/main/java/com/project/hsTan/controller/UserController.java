package com.project.hsTan.controller;

import com.project.hsTan.server.UserServer;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    @Resource
    private UserServer userServer;

    @GetMapping("test")
    public List<String> test() {
        List<String> result = userServer.test();
        if (CollectionUtils.isEmpty(result)) {
            return Collections.singletonList("查询为空！");
        }
        return result;
    }
}
